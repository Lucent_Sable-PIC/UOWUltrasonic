/*
 * UOW Mechatronics Assignment 1
 * Ultrasonic Rangefinder/Altimeter
 */

#include <xc.h>
#define _XTAL_FREQ 4000000


#pragma config FOSC=INTRCIO
#pragma config WDTE=OFF
#pragma config PWRTE=OFF
#pragma config MCLRE=OFF
#pragma config BOREN=OFF

static inline void init(void)
{	
	//Set pin GP5, GP1, GP0 as outputs, all others as inputs
	TRISIO = ~0x23;
	//set all pins as digital, except GP4(AN3) which is analog
	ANSEL = 0x18;
	ADCON0 = 0x8D;
	//disable comparitor module
	CMCON = 7;
	asm("nop");
	
	//set up the timer module
	T1CON = 0x00;
	//enable the timer1 interrupt
	PIE1bits.TMR1IE = 1;
	INTCONbits.PEIE = 1;
}

/* Pulse: activate the Ultrasonic Transducer at 40kHz
 * This operates, knowing that each instruction is approx 1us, and 25us is the desired period
*/
char cycles;
static void pulse()
{
	char CNT;
#asm
	//Set up the port correctly, 0BXXXXXX01
	MOVLW	0xFC
	ANDWF	GPIO,F
	NOP
	INCF	GPIO,F
	NOP
pulseStart: //start loop
	// SET to 10
	INCF	GPIO,F
	//need 12 instructions total, 11 after INCF
	CALL four
	CALL four
	NOP
	NOP
	NOP
	//Set to 01
	DECF	GPIO,F
	//need 13 instructions, 12 after DECF
	CALL four
	CALL four
	NOP
	DECFSZ	_cycles,F
	GOTO	pulseStart
#endasm
//if this return was melded with four's, then the stack would be damaged
return;
#asm
	//FOUR is a simple 4 instruction delay, call and return
	//this is more compact than 4 nop instructions.
four: return
#endasm
}

volatile int timeout;
void main(void)
{
	int result = 0;
	init();
	//set all outputs low
	GPIO = 0x00;
	
	while(1)
	{
		//Toggle the GP5 pin
		GPIO ^= 0x20;
		/*
		//Delay for a set number of ms, according to the potentiometer
		//get a reading from the potentiometer
		ADCON0bits.CHS=3;
		ADCON0bits.GO = 1;
		//wait for the ADC to complete
		while(ADCON0bits.GO);
		//set the length of the delay using the ADC value.
		//add 100 to ensure that there is some noticable delay
		result = ((unsigned int)ADRESH<<8) + ADRESL+100;
		//delay one ms delay_time times.
		//note that __delay_[ms/us] is a macro definition, and is evaluated
		//at compile time, not run time
		while(result--)
		{
			__delay_us(997);
		}*/
		
	
		INTCONbits.GIE = 1;
		timeout = 0;
		TMR1H = 0xDC;
		TMR1L = 0xD7;
		cycles = 10;
		pulse();
		T1CONbits.TMR1ON = 1;
		//after the pulse is transmitted, start sampling, looking for a 3.2v (163)
		//adc reading. This should be a valid wave.
		ADCON0bits.CHS = 2;
		do
		{
			ADCON0bits.GO = 1;
			while(ADCON0bits.GO);
			result = ((unsigned int)ADRESH<<8) + ADRESL;
		}while(result < 655 && !timeout);
		T1CONbits.TMR1ON =0;
		if(!timeout)
		{
			result = ((unsigned int)TMR1H<<8) + TMR1L;
			result = result-0xDCD7;
		}
		else
		{
			result = 500;
		}
		//wait for the timer to overflow
		INTCONbits.GIE = 0;
		//at this point, we have received a voltage greater than 3.2V on the receiver pin.
		//this is where we would take the delay time, and use it to calculate the distance
		while(result--)
		{
			__delay_us(997);
		}
	}
}


void interrupt ISR(void)
{
	if(PIR1bits.TMR1IF)
	{
		timeout = 1;
		PIR1bits.TMR1IF = 0;
		T1CONbits.TMR1ON = 0;
	}
}