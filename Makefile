SHELL := Powershell.exe

CC = xc8

PROCESSOR = 12F675



all: build/app.hex

###Translate###
build/app.hex: src/app.c
	cd build; $(CC) --chip=$(PROCESSOR) -m  ../src/app.c

#build/translate.hex: build/translate.elf
#	$(TBH) build/translate.elf

clean:
	rm build/*
	rm debug/*